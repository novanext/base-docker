#-------------------------------------------------------
# Debian 12.8 was released on November 9th, 2024. Debian 12.0 was initially released on June 10th, 2023. 
FROM debian:bookworm AS base
# platform specification is needed for ARM systems, such as Apple Silicon
# FROM --platform=linux/amd64 debian:bookworm AS base
#-------------------------------------------------------

  ENV DEBIAN_FRONTEND=noninteractive
  ENV PATH="/usr/local/texlive/2023/bin/x86_64-linux:${PATH}"
  ENV TEXLIVE_URL="https://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2023/"
  ENV DISPLAY=host.docker.internal:0.0

  # collection for a package can be found by searching, e.g. for "depend siunitx"
  # https://www.ctan.org/tex-archive/systems/texlive/tlnet/tlpkg/texlive.tlpdb

  RUN apt-get update \
    && apt-get install -y \
    cabextract \
    fonts-noto \
    fonts-noto-cjk \
    fonts-roboto \
    git \
    imagemagick \
    latexmk \
    libfontconfig \
    librsvg2-bin \
    locate \
    lyx \
    make \
    perl \
    poppler-utils \
    python3 \
    python3-click \
    python3-pip \
    python3-pygments \
    python3-pypdf2 \
    python3-venv \
    python-is-python3 \
    sudo \
    vim \
    wget

  RUN apt-get clean \
    && apt-get --purge remove -y .\*-doc$ \
    && rm -rf /var/lib/apt/lists/*

  RUN wget https://www.freedesktop.org/software/fontconfig/webfonts/webfonts.tar.gz\
    && tar -xzf webfonts.tar.gz && cd msfonts && cabextract *.exe && cd ..\
    && cp -r msfonts /usr/share/fonts/truetype

  # Install Texlive
  RUN wget $TEXLIVE_URL/install-tl-unx.tar.gz\
    && tar -zxf install-tl-unx.tar.gz\
    && rm install-tl-unx.tar.gz\
    && cd install-tl-*\
    && ./install-tl --scheme scheme-small --no-interaction --repository $TEXLIVE_URL/tlnet-final/


  # Install Texlive packages
  RUN tlmgr install \
    collection-bibtexextra \
    collection-fontsrecommended \
    collection-fontsextra \
    collection-langenglish \
    collection-langeuropean \
    collection-langgreek \
    collection-latexrecommended \
    collection-latexextra \
    collection-xetex \
    collection-luatex \
    collection-mathscience \
    cm-super

# build locally and use interactively as:
# docker build -t tud-dissertation .
# docker run -it --rm -v $(pwd):/code tud-dissertation bash

# or download from gitlab and use locally:
# docker run -it --rm -v $(pwd):/code --platform=linux/amd64 registry.gitlab.com/novanext/tudelft-dissertation:master

# To create a user (for GUI applications)
# useradd -m -G sudo novanext
# echo 'novanext:novanext' | chpasswd
