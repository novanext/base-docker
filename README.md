# base docker

Ubuntu image with LaTeX and Python installed, so that specific images can be built quicker

If the runner provided by Gitlab is too slow, these commands can be used to build the image locally:
<https://gitlab.com/novanext/base-docker/container_registry>

(
	in mac you might need to delete `"credsStore" : "desktop",` from `~/.docker/config.json`
	<https://github.com/docker/docker-credential-helpers/issues/149>
)

```shell
docker login registry.gitlab.com
docker build -t registry.gitlab.com/novanext/base-docker:$(git branch --show-current) .
docker push registry.gitlab.com/novanext/base-docker:$(git branch --show-current)
```
